package br.com.agenda;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner captura = new Scanner(System.in);
	    Agenda agendaContatos = new Agenda();

        CapturaSolicitacao.capturaInsercao(agendaContatos);

        agendaContatos.mostrarAgenda();

        if(CapturaSolicitacao.capturaDesejoRemover()){
            CapturaSolicitacao.capturaRemocao(agendaContatos);
            agendaContatos.mostrarAgenda();
        }
    }
}
