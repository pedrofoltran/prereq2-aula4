package br.com.agenda;

import java.util.Scanner;

public class CapturaSolicitacao {
    public static boolean capturaDesejoRemover(){
        Scanner captura = new Scanner(System.in);
        System.out.println("Deseja remover algum contato? S/n");
        String resposta = captura.next();
        if (resposta.equals("S") || resposta.equals('s')){
            return true;
        } else {
            return false;
        }
    }

    public static void capturaRemocao(Agenda agenda){

        Scanner captura = new Scanner(System.in);
        System.out.println("Deseja por email? S/n");
        String resposta = captura.next();

        if (resposta.equals("S") || resposta.equals('s')){
            System.out.println("Digite email: ");
            String email = captura.next();
            agenda.removerContatoEmail(email);
        } else {
            System.out.println("Digite telefone: ");
            String telefone = captura.next();
            agenda.removerContatoTelefone(telefone);
        }
    }

    public static void capturaInsercao(Agenda agenda){
        Scanner captura = new Scanner(System.in);
        System.out.println("Digite a quantidade de contatos que quer adicionar: ");
        agenda.adicionarVariasPessoas(captura.nextInt());
    }

}
