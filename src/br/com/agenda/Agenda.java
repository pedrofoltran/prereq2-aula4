package br.com.agenda;

import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class Agenda {
    List<Contato> agenda = new LinkedList<Contato>();
    public Agenda(){}

    public void adicionarContato(Contato pessoa){
        agenda.add(pessoa);
    }

    public void removerContatoTelefone(String telefone){
        int i;
        for(i = 0; !agenda.get(i).getTelefone().equals(telefone);i++){
        }
        agenda.remove(i);
    }

    public void removerContatoEmail(String email){
        int i;
        for(i = 0; !agenda.get(i).getEmail().equals(email);i++){
        }
        agenda.remove(i);
    }

    public void adicionarVariasPessoas(int quantidade){
        Scanner captura = new Scanner(System.in);

        for(int i = 0; i<quantidade; i++){
            System.out.println("Digite nome do contato:");
            Contato novoContato = new Contato(captura.next());
            System.out.println("Digite email do contato:");
            novoContato.setEmail(captura.next());
            System.out.println("Digite telefone do contato");
            novoContato.setTelefone(captura.next());
            this.adicionarContato(novoContato);
        }
    }

    public void mostrarAgenda(){
        for(Contato pessoa: agenda){
            System.out.println("Contato nome: " + pessoa.getNome() + ", telefone "+ pessoa.getTelefone() + " email " + pessoa.getEmail());
        }
    }
}
