package br.com.banco;

import java.sql.SQLOutput;
import java.util.Random;

public class Teste {

    public static void passouTesteCriacaoCliente(int quantidadeTestes){
        int erro = 0;
        for(int i = 0; i < quantidadeTestes; i++){
            if(!Teste.testeCriacaoCliente()){
                erro += 1;
            }
        }

        if (erro != 0){
            System.out.println("ERRO! Não passou nos teste de criação");
        } else {
            System.out.println("PASSOU! Teste de criação");
        }
    }

    public static void passouTesteDeposito(int quantidadeTestes,Conta contaTeste){
        int erro = 0;
        for(int i = 0; i < quantidadeTestes; i++){
            if(!Teste.testeDeposito(contaTeste)){
                erro += 1;
            }
        }

        if (erro != 0){
            System.out.println("ERRO! Não passou nos teste de deposito");
        } else {
            System.out.println("PASSOU! Teste de deposito");
        }
    }

    public static void passouTesteSaque(int quantidadeTestes, Conta contaTeste){
        int erro = 0;

        for(int i = 0; i < quantidadeTestes; i++){
            if(!Teste.testeSaque(contaTeste)){
                erro += 1;
            }
        }

        if (erro != 0) {
            System.out.println("ERRO! Não passou nos teste de saque");
        } else {
            System.out.println("PASSOU! Teste de saque");
        }
    }

    public static boolean testeCriacaoCliente(){
        String nome;
        String[] nomes = {"Pedro", "Odair", "Diego", "Paulo", "Angela", "Anna", "Marcia", "Leticia"};
        int idade;

        Random aleatorio = new Random();
        idade = aleatorio.nextInt(8) + 15;
        nome = nomes[aleatorio.nextInt(8)];

        try{
            Cliente novoCliente = new Cliente(nome, idade);
        } catch (ArithmeticException e) {
            if(e.getMessage().equals("Menor de Idade") && idade < 18){
                return true;
            } else
            {
                return false;
            }
        }
        return true;
    }

    public static boolean testeSaque(Conta contaCliente){
        Random aleatorio = new Random();
        double saldo;
        boolean retornoFuncao;

        saldo = contaCliente.getSaldo();

        if(aleatorio.nextBoolean()){
            retornoFuncao = contaCliente.sacarDinheiro(saldo-10);
            if (retornoFuncao == true)
                return true;
        } else {
            retornoFuncao = contaCliente.sacarDinheiro(saldo+10);
            if (retornoFuncao == false)
                return true;
        }
        return false;
    }

    public static boolean testeDeposito(Conta contaCliente){
        double saldoNovo;
        double saldoAntigo;
        double deposito;
        Random aleatorio = new Random();

        saldoAntigo = contaCliente.getSaldo();
        deposito = aleatorio.nextInt(30000);


        contaCliente.depositarDinheiro(deposito);
        saldoNovo = contaCliente.getSaldo();

//        System.out.println("Saldo antigo: " + saldoAntigo);
//        System.out.println("Deposito: "+ deposito);
//        System.out.println("Saldo novo: "+ saldoNovo);
        saldoAntigo += deposito;

        if (saldoAntigo != saldoNovo){
            return false;
        }
        return true;
    }
}
