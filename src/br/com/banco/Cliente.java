package br.com.banco;

public class Cliente {
    private int idade;
    private String nome;
    private String erro;

    public Cliente(String nome, int idade){
        if (idade >= 18) {
            this.nome = nome;
            this.idade = idade;
            this.erro = "";
        } else {
            throw new ArithmeticException("Menor de Idade");
        }
    }

    public String getErro() {
        return erro;
    }

    public int getIdade() {
        return idade;
    }

    public String getNome() {
        return nome;
    }
}
