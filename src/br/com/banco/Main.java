package br.com.banco;

public class Main {
    public static void main(String[] args) {
        int quantidadeTestes = 40;

        Teste.passouTesteCriacaoCliente(quantidadeTestes);


        Cliente clienteTeste = new Cliente("pedro", 30);
        Conta contaTeste = new Conta(clienteTeste);

        Teste.passouTesteDeposito(quantidadeTestes, contaTeste);
        Teste.passouTesteSaque(quantidadeTestes, contaTeste);
    }

}
