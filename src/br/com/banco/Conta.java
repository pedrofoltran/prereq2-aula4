package br.com.banco;

public class Conta {
    private double saldo;
    private Cliente cliente;

    public Conta(Cliente cliente){
        this.cliente = cliente;
        this.saldo = 0;
    }
    public boolean sacarDinheiro(double valor){
        if(valor < saldo){
            saldo -= valor;
            return true;
        }
        return false;
    }

    public void depositarDinheiro(double valor){
        saldo += valor;
    }

    public double getSaldo() { return saldo; }




}
